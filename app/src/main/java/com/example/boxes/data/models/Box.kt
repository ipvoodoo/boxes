package com.example.boxes.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.boxes.ui.util.ConstantManager

@Entity(tableName = ConstantManager.DATABASE_TABLE)
data class Box(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val title: String,
    val description: String,
    val priority: Priority
)