package com.example.boxes.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.boxes.data.models.Box

@Database(entities = [Box::class], version = 1, exportSchema = false)
abstract class BoxesDatabase :RoomDatabase(){

    abstract fun boxesDao():BoxesDao
}