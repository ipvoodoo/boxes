package com.example.boxes.data

import androidx.room.*
import com.example.boxes.data.models.Box
import kotlinx.coroutines.flow.Flow

@Dao
interface BoxesDao {

    @Query("SELECT * FROM boxes_table ORDER BY id ASC")
    fun getAllBoxes(): Flow<List<Box>>

    @Query("SELECT * FROM boxes_table WHERE id = :boxId")
    fun getSelectedBox(boxId: Int): Flow<Box>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addBox(box: Box)

    @Update
    suspend fun updateBox(box: Box)

    @Delete
    suspend fun deleteBox(box: Box)

    @Query("DELETE FROM boxes_table")
    suspend fun deleteAllBoxes()

    @Query("SELECT * FROM boxes_table WHERE title LIKE :searchQuery OR description LIKE :searchQuery")
    fun searchDatabase(searchQuery: String): Flow<List<Box>>
}