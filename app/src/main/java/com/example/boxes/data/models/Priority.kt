package com.example.boxes.data.models

import androidx.compose.ui.graphics.Color
import com.example.boxes.ui.theme.HighPriorityColor
import com.example.boxes.ui.theme.LowPriorityColor
import com.example.boxes.ui.theme.MediumPriorityColor
import com.example.boxes.ui.theme.NonePriorityColor

enum class Priority(val color: Color) {
    HIGH(HighPriorityColor),
    MEDIUM(MediumPriorityColor),
    LOW(LowPriorityColor),
    NONE(NonePriorityColor)
}