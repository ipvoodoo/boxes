package com.example.boxes.data.repositories

import com.example.boxes.data.BoxesDao
import com.example.boxes.data.models.Box
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@ViewModelScoped
class BoxesRepository @Inject constructor(private val boxesDao: BoxesDao) {

    val getAllBoxes: Flow<List<Box>> = boxesDao.getAllBoxes()

    fun getSelectedBox(boxId: Int): Flow<Box> {
        return boxesDao.getSelectedBox(boxId = boxId)
    }

    suspend fun addBox(box: Box) {
        boxesDao.addBox(box = box)
    }

    suspend fun deleteBox(box: Box) {
        boxesDao.deleteBox(box = box)
    }

    suspend fun deleteAllBoxes() {
        boxesDao.deleteAllBoxes()
    }

    fun searchDatabase(searchQuery: String): Flow<List<Box>> {
        return boxesDao.searchDatabase(searchQuery = searchQuery)
    }
}