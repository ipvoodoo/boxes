package com.example.boxes.di

import android.content.Context
import androidx.room.Room
import com.example.boxes.data.BoxesDatabase
import com.example.boxes.ui.util.ConstantManager
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    @Singleton
    @Provides
    fun provideDatabase(
        @ApplicationContext context: Context
    ) = Room.databaseBuilder(
        context,
        BoxesDatabase::class.java,
        ConstantManager.DATABASE_NAME
    ).build()

    @Singleton
    @Provides
    fun provideDao(database: BoxesDatabase) = database.boxesDao()
}