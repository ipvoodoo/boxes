package com.example.boxes

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class BoxApplication : Application() {
}