package com.example.boxes.ui.viewmodels

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.boxes.data.models.Box
import com.example.boxes.data.repositories.BoxesRepository
import com.example.boxes.ui.util.RequestState
import com.example.boxes.ui.util.SearchAppBarState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SharedViewModel @Inject constructor(
    private val repository: BoxesRepository
) : ViewModel() {

    val searchAppBarState: MutableState<SearchAppBarState> =
        mutableStateOf(SearchAppBarState.CLOSED)

    val searchTextState: MutableState<String> =
        mutableStateOf("")

    private val _allBoxes = MutableStateFlow<RequestState<List<Box>>>(RequestState.Idle)
    val allBoxes: StateFlow<RequestState<List<Box>>> = _allBoxes

    fun getAllBoxes() {
        _allBoxes.value = RequestState.Loading
        try {
            viewModelScope.launch {
                repository.getAllBoxes.collect { boxes ->
                    _allBoxes.value = RequestState.Success(boxes)
                }
            }
        } catch (e: Exception) {
            _allBoxes.value = RequestState.Error(e)
        }
    }
}