package com.example.boxes.ui.util

object ConstantManager {
    const val DATABASE_TABLE = "boxes_table"
    const val DATABASE_NAME = "boxes_database"

    const val BOX_LIST_SCREEN = "list/{action}"
    const val BOX_SCREEN = "box/{boxId}"

    const val BOX_LIST_ARGUMENT_KEY = "action"
    const val BOX_ARGUMENT_KEY = "boxId"
}