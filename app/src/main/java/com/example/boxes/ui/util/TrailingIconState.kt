package com.example.boxes.ui.util

enum class TrailingIconState {
    READY_TO_DELETE,
    READY_TO_CLOSE
}