package com.example.boxes.ui.util

enum class SearchAppBarState {
    OPENED,
    CLOSED,
    TRIGGERED
}