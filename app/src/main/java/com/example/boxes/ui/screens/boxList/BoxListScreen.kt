package com.example.boxes.ui.screens.boxList

import androidx.compose.material.FloatingActionButton
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.res.stringResource
import com.example.boxes.R
import com.example.boxes.ui.theme.LightTextColor
import com.example.boxes.ui.theme.fabBackgroundColor
import com.example.boxes.ui.util.SearchAppBarState
import com.example.boxes.ui.viewmodels.SharedViewModel

@Composable
fun BoxListScreen(
    navigateToBoxScreen: (boxId: Int) -> Unit,
    sharedViewModel: SharedViewModel
) {

    LaunchedEffect(key1 = true) {
        sharedViewModel.getAllBoxes()
    }

    val allBoxes by sharedViewModel.allBoxes.collectAsState()

    val searchAppBarState: SearchAppBarState by sharedViewModel.searchAppBarState
    val searchTextState: String by sharedViewModel.searchTextState

    Scaffold(
        topBar = {
            ListAppBar(
                sharedViewModel = sharedViewModel,
                searchAppBarState = searchAppBarState,
                searchTextState = searchTextState
            )
        },
        content = {
            ListContent(
                boxes = allBoxes,
                navigateToBoxScreen = navigateToBoxScreen)
        },
        floatingActionButton = {
            ListFab(onFabClicked = navigateToBoxScreen)
        }
    )
}

@Composable
private fun ListFab(
    onFabClicked: (boxId: Int) -> Unit
) {
    FloatingActionButton(
        onClick = {
            onFabClicked(-1)
        },
        backgroundColor = MaterialTheme.colors.fabBackgroundColor
    ) {
        Icon(
            imageVector = Icons.Filled.Add,
            contentDescription = stringResource(
                id = R.string.add_button
            ),
            tint = LightTextColor,
        )
    }
}