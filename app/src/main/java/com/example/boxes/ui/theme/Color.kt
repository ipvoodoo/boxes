package com.example.boxes.ui.theme

import androidx.compose.material.Colors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

val LightTextColor = Color(0xFFFFFFFF)
val MediumTextColor = Color(0xFF757575)
val DarkTextColor = Color(0xFF212121)
val AccentColor = Color(0xFFFF5722)
val PrimaryColor = Color(0xFF607D8B)
val DarkPrimaryColor = Color(0xFF455A64)
val LightPrimaryColor = Color(0xFFCFD8DC)
val DividerColor = Color(0xFFBDBDBD)

val LowPriorityColor = Color(0xFF00C980)
val MediumPriorityColor = Color(0xFFFFC114)
val HighPriorityColor = Color(0xFFFF4646)
val NonePriorityColor = Color(0xFFFFFFFF)

val Colors.boxItemTextColor: Color
    @Composable
    get() = if (isLight) DarkPrimaryColor else LightTextColor

val Colors.boxItemBackgroundColor: Color
    @Composable
    get() = if (isLight) Color.White else LightPrimaryColor

val Colors.topAppBarContentColor: Color
    @Composable
    get() = if (isLight) Color.White else LightPrimaryColor

val Colors.topAppBarBackgroundColor: Color
    @Composable
    get() = if (isLight) PrimaryColor else DarkPrimaryColor

val Colors.fabBackgroundColor: Color
    @Composable
    get() = if (isLight) AccentColor else AccentColor