package com.example.boxes.ui.screens.boxList

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import com.example.boxes.data.models.Box
import com.example.boxes.data.models.Priority
import com.example.boxes.ui.theme.*
import com.example.boxes.ui.util.RequestState

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun ListContent(
    boxes: RequestState<List<Box>>,
    navigateToBoxScreen: (boxId: Int) -> Unit
) {
    if (boxes is RequestState.Success) {
        if (boxes.data.isEmpty()) {
            EmptyContent()
        } else {
            DisplayBoxes(
                boxes = boxes.data,
                navigateToBoxScreen = navigateToBoxScreen
            )
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun DisplayBoxes(
    boxes: List<Box>,
    navigateToBoxScreen: (boxId: Int) -> Unit
) {
    LazyColumn {
        items(
            items = boxes,
            key = { box ->
                box.id

            }
        ) { box ->
            BoxItem(
                box = box,
                navigateToBoxScreen = navigateToBoxScreen
            )
        }
    }
}

@ExperimentalMaterialApi
@Composable
fun BoxItem(
    box: Box,
    navigateToBoxScreen: (boxId: Int) -> Unit
) {
    Surface(
        modifier = Modifier
            .fillMaxWidth(),
        color = MaterialTheme.colors.boxItemBackgroundColor,
        shape = RectangleShape,
        elevation = BOX_ITEM_ELEVATION,
        onClick = {
            navigateToBoxScreen(box.id)
        }
    ) {
        Column(
            modifier = Modifier
                .padding(all = LARGE_PADDING)
                .fillMaxWidth()
        ) {
            Row {
                Text(
                    modifier = Modifier.weight(8f),
                    text = box.title,
                    color = MaterialTheme.colors.boxItemTextColor,
                    style = MaterialTheme.typography.h5,
                    fontWeight = FontWeight.Bold,
                    maxLines = 1
                )
                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .weight(1f),
                    contentAlignment = Alignment.TopEnd
                ) {
                    Canvas(
                        modifier = Modifier
                            .size(PRIORITY_INDICATOR_SIZE)
                    ) {
                        drawCircle(
                            color = box.priority.color
                        )
                    }
                }
            }
            Text(
                modifier = Modifier.fillMaxWidth(),
                text = box.description,
                color = MaterialTheme.colors.boxItemTextColor,
                style = MaterialTheme.typography.subtitle1,
                maxLines = 2,
                overflow = TextOverflow.Ellipsis
            )
        }
    }
}

@ExperimentalMaterialApi
@Composable
@Preview
fun BoxItemPreview() {
    BoxItem(
        box = Box(
            id = 0,
            title = "Title",
            description = "eirbf,eiurbfgebr",
            priority = Priority.MEDIUM
        ),
        navigateToBoxScreen = {})
}