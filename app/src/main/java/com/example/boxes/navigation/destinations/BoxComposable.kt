package com.example.boxes.navigation.destinations

import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavType
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.example.boxes.ui.util.Action
import com.example.boxes.ui.util.ConstantManager

fun NavGraphBuilder.boxComposable(
    navigateToBoxListScreen: (Action) -> Unit
) {
    composable(
        route = ConstantManager.BOX_SCREEN,
        arguments = listOf(navArgument(ConstantManager.BOX_ARGUMENT_KEY) {
            type = NavType.IntType
        })
    ) {

    }
}