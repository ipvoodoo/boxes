package com.example.boxes.navigation

import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import com.example.boxes.navigation.destinations.boxComposable
import com.example.boxes.navigation.destinations.boxListComposable
import com.example.boxes.ui.util.ConstantManager
import com.example.boxes.ui.viewmodels.SharedViewModel

@Composable
fun SetupNavigation(
    navController: NavHostController,
    sharedViewModel: SharedViewModel
) {
    val screen = remember(navController) {
        Screens(navController = navController)
    }

    NavHost(navController = navController, startDestination = ConstantManager.BOX_LIST_SCREEN) {
        boxListComposable(
            navigateToBoxScreen = screen.box,
            sharedViewModel = sharedViewModel
        )
        boxComposable(navigateToBoxListScreen = screen.boxList)
    }
}