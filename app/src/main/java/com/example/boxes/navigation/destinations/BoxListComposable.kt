package com.example.boxes.navigation.destinations

import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavType
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.example.boxes.ui.screens.boxList.BoxListScreen
import com.example.boxes.ui.util.ConstantManager
import com.example.boxes.ui.viewmodels.SharedViewModel

fun NavGraphBuilder.boxListComposable(
    navigateToBoxScreen: (boxId: Int) -> Unit,
    sharedViewModel: SharedViewModel
) {
    composable(
        route = ConstantManager.BOX_LIST_SCREEN,
        arguments = listOf(navArgument(ConstantManager.BOX_LIST_ARGUMENT_KEY) {
            type = NavType.StringType
        })
    ) {
        BoxListScreen(
            navigateToBoxScreen = navigateToBoxScreen,
            sharedViewModel = sharedViewModel
        )
    }
}