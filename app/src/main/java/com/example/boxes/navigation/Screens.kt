package com.example.boxes.navigation

import androidx.navigation.NavController
import com.example.boxes.ui.util.Action
import com.example.boxes.ui.util.ConstantManager

class Screens(navController: NavController) {
    val boxList: (Action) -> Unit = { action ->
        navController.navigate("list/${action.name}") {
            popUpTo(ConstantManager.BOX_LIST_SCREEN) {
                inclusive = true
            }
        }
    }

    val box: (Int) -> Unit = { boxId ->
        navController.navigate("box/$boxId")
    }
}